`Install`

npm i radial-slider --save-dev

`Using`

**js** 

import 'radial-slider';

$(`parent block`).rnSlider({maxSectors: 7});

**css** 

@import "~radial-slider/src/sass/radial-slider";
