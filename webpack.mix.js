let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .autoload({
        jquery: ['$', 'window.jQuery', 'jQuery'],
        lodash: ['_', 'window._']
    })
    .extract(['jquery', 'bootstrap', 'lodash'])
    .js('src/js/app.js', 'public/js')
    .sass('src/sass/app.scss', 'public/css')
    .copy('src/js/modernizr.min.js', 'public/js/modernizr.min.js')
    .copy('src/index.html', 'public/index.html')
    .copy('src/sass/*.css', 'public/css')
;
