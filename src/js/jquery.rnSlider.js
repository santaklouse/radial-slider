/**
 * Radial Navigation Slider
 * jquery.rnSlider.js v1.0.0
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright 2018
 */

import "./jquery.windy.js";
import WatchJS from 'melanke-watchjs';
import TweenLite from "gsap/TweenLite";

var logError = function( message ) {
    window.console && window.console.error( message );
};

/*
Clock HTML template:

<div class="pager">
    <div class="faceBox" style="transform: rotate(30deg);">
        <div class="slideNumber" style="transform: rotate(-30deg);">1</div>
    </div>
    <div class="faceBox" style="transform: rotate(60deg);">
        <div class="slideNumber" style="transform: rotate(-60deg);">2</div>
    </div>
    <div class="faceBox" style="transform: rotate(90deg);">
        <div class="slideNumber" style="transform: rotate(-90deg);">3</div>
    </div>
    <div class="faceBox" style="transform: rotate(120deg);">
        <div class="slideNumber" style="transform: rotate(-120deg);">4</div>
    </div>
    <div class="faceBox" style="transform: rotate(150deg);">
        <div class="slideNumber" style="transform: rotate(-150deg);">5</div>
    </div>
    <div class="faceBox" style="transform: rotate(180deg);">
        <div class="slideNumber" style="transform: rotate(-180deg);">6</div>
    </div>
    <div class="faceBox" style="transform: rotate(210deg);">
        <div class="slideNumber" style="transform: rotate(-210deg);">7</div>
    </div>
    <div class="faceBox" style="transform: rotate(240deg);">
        <div class="slideNumber" style="transform: rotate(-240deg);">8</div>
    </div>
    <div class="faceBox" style="transform: rotate(270deg);">
        <div class="slideNumber" style="transform: rotate(-270deg);">9</div>
    </div>
    <div class="faceBox" style="transform: rotate(300deg);">
        <div class="slideNumber wide" style="transform: rotate(-300deg); left: -0.3em;">10</div>
    </div>
    <div class="faceBox" style="transform: rotate(330deg);">
        <div class="slideNumber wide" style="transform: rotate(-330deg); left: -0.3em;">11</div>
    </div>
    <div class="faceBox" style="transform: rotate(360deg);">
        <div class="slideNumber wide" style="transform: rotate(-360deg);">12</div>
    </div>
    </div>
*/

;( function( $, window, undefined ) {

    'use strict';

    $.rnSlider = function (options, element) {
        this.$el = $(element);
        this._init(options);
    };

    // the options
    $.rnSlider.defaults = {
        padding: 2,
        maxSectors: 6,
        // if we want to specify a selector that triggers the next() function. example: '#wi-nav-next'.
        nextEl: '',
        // if we want to specify a selector that triggers the prev() function.
        prevEl: '',
        // rotation and translation boundaries for the items transitions
        boundaries : {
            rotateX : { min : 40 , max : 90 },
            rotateY : { min : -15 , max : 15 },
            rotateZ : { min : -15 , max : 0 },
            translateX : { min : -150 , max : -100 },
            translateY : { min : -200 , max : -150 },
            translateZ : { min : 250 , max : 450 }
        }
    };

    $.rnSlider.prototype = {
        _navigationBody: function() {
            return $(
                '<nav>' +
                    '<a href="#" class="prev"></a>' +
                    '<a href="#" class="next"></a>' +
                '</nav>'
            );
        },

        watch: WatchJS.watch,

        _compileAndInitSlider: function() {
            this.$el
                .addClass('rn-slider d-flex justify-content-center mb-5')
                .children()
                .not('.p-0.m-0')
                .wrapAll($('<div>', { class: 'rn-slider-content p-0 m-0' }))
                .wrapAll($('<ul>', { class: 'wi-container' }))
                .wrap($('<li>', { class:"wi-list-element" }))
                .closest('.rn-slider')
                .find('.rn-slider-content')
                .append(this._navigationBody())
                .prepend($('<div>', {class: 'p-0 m-0', html: '<div class="pager"></div>'}))
            ;

            this._windy = $('ul.wi-container', this.$el).windy(this.options);
            this.$pager = $('.pager', this.$el).append($('<div/>', {class: 'active'}));
            $('.active', this.$pager).html(this._buildRoundPager());
            return this;
        },

        _init: function (options) {
            this.options = $.extend(true, {}, $.rnSlider.defaults, options);

            return this
                ._compileAndInitSlider()
                ._initEvents();
        },

        _initEvents: function() {
            this.watch(this._windy, "current", (prop, action, newVal, oldVal) => {
                if (newVal === oldVal) {
                    return;
                }
                this.navigate(parseInt(newVal, 10));
            });
            this.$el.on('click', 'nav a', (e) => {
                e.preventDefault();
                let isNext = $(e.currentTarget).hasClass('next');

                if (isNext) {
                    return this.next();
                }
                this.prev();
            });

            this.$el.on('click', '.slideNumber', (e) => {
                e.preventDefault();
                this.navigate(parseInt($(e.currentTarget).data('slide-number'), 10));
            });
            return this;
        },

        // public method: shows next item
        next : function () {
            if( this.current() < this.total() - 1 ) {
                this.navigate( this.current() + 1 );
            } else {
                this.navigate(0);
            }
            return this;
        },
        // public method: shows previous item
        prev : function () {
            if( this.current() > 0 ) {
                this.navigate( this.current() - 1 );
            } else {
                this.navigate(this.total() - 1);
            }
        },

        /*
         * public method: shows previous item
         * @return int
         */
        current: function() {
            return this._windy.current;
        },

        total: function() {
            return this._windy.getItemsCount();
        },

        _calculatePages: function(currentPage) {
            let options = this.options;
            let padding = options.padding,
                totalPages = this.total(),
                start = 0 - options.padding,
                end = options.maxSectors - options.padding;

            currentPage = currentPage === undefined ? this.current() : currentPage;
            end = end >= totalPages - currentPage ? totalPages - currentPage : end;

            var calculateSeparatorLink = (i) => {
                return (currentPage - i) +
                padding +
                Math.ceil(
                    (totalPages - (currentPage + i) + padding) / 2
                );
            };
            return _.chain(_.range(start, end))
                .map(function(i) {
                    let pageNum = i + currentPage + 1,
                        isCurrent = i === 0,
                        skip = pageNum <= 0;

                    if (skip) {
                        return;
                    }
                    let page = {
                        currentPage: currentPage,
                        sector: i + padding + 1,
                        i: i,
                        l: end,
                        pageNum: pageNum,
                        isCurrent: isCurrent,
                        skip: skip
                    };

                    let shouldBeGrouped = totalPages - currentPage + 1 > end;
                    let isSeparator = shouldBeGrouped && i === (end - 2) && pageNum < totalPages - 1;

                    if (i === end - 1) {
                        page.link = totalPages - 1;
                        page.text = totalPages;
                    } else if (isSeparator) {
                        //calculating page number that should be chosen
                        //if user clicked on grouped pages -> '...'
                        page.link = calculateSeparatorLink(i);
                        page.text = '&hellip;';
                    } else {
                        page.text = pageNum;
                        page.link = pageNum - 1;
                    }

                    page.link = page.link > totalPages - 1 ? totalPages - 1 : page.link;
                    return page;
                })
                .compact()
                .value();
        },

        navigate: function(pageNum) {
            if (pageNum === undefined || pageNum === this.current()) {
                return;
            }

            pageNum = parseInt(pageNum, 10);

            //TODO: remove this line after finish animation
            $('.active', this.$pager).html(this._buildRoundPager(pageNum));

            /*
            //animation
                let $old = $('.active', this.$pager),
                    $new = $('<div/>', {
                        class: 'new',
                        html: this._buildRoundPager(pageNum)
                    }).appendTo(this.$pager);

                TweenLite.to($new, 1, {
                    onComplete: () => {
                        $old.remove();
                        $new.removeClass('new').addClass('active');
                    }
                });
            */

            this._windy.navigate(pageNum);
            return this;
        },

        _buildRoundPager: function(pageNumber) {
            return _.map(this._calculatePages(pageNumber), function(item) {
                let $faceBox,
                    deg = 30 * item.sector,
                    $classes = [
                        'faceBox',
                        item.class,
                        item.isCurrent && 'current',
                        `rn-sector-${item.sector}`
                    ];

                $faceBox = $('<div/>', {
                    class: _.compact($classes).join(' ')
                })
                    .css('transform', `rotate(${deg}deg)`)
                ;

                $classes = ['slideNumber', item.link > 8 && 'wide'];
                $('<a/>', {
                    href: '#',
                    class: _.compact($classes).join(' '),
                    html: item.text
                })
                    .data('slide-number', String(item.link))
                    .css('transform', `rotate(-${deg}deg)`)
                    .appendTo($faceBox)
                ;
                return $faceBox;
            });
        },
    };
    $.fn.rnSlider = function( options ) {
        var instance = $.data( this, 'rnSlider' );
        if ( typeof options === 'string' ) {
            var args = Array.prototype.slice.call( arguments, 1 );
            this.each(function() {
                if ( !instance ) {
                    logError( "cannot call methods on rnSlider prior to initialization; " +
                        "attempted to call method '" + options + "'" );
                    return;
                }
                if ( !$.isFunction( instance[options] ) || options.charAt(0) === "_" ) {
                    logError( "no such method '" + options + "' for rnSlider instance" );
                    return;
                }
                instance[ options ].apply( instance, args );
            });
        }
        else {
            this.each(function() {
                if ( instance ) {
                    instance._init();
                }
                else {
                    instance = $.data( this, 'rnSlider', new $.rnSlider( options, this ) );
                }
            });
        }
        return instance;
    };
}(jQuery, window));
